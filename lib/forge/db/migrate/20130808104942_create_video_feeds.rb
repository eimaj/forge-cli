class CreateVideoFeeds < ActiveRecord::Migration
  def up
    create_table "video_feeds" do |t|
      t.string   "title"
      t.string   "channel"
      t.string   "video_id"
      t.string   "thumbnail_url"
      t.string   "source"
      t.boolean  "published",     :default => true
      t.datetime "published_at"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end

  def down
    drop_table :video_feeds
  end
end
