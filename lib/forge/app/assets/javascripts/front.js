//= require jquery
//= require jquery_ujs
//= require application
//= require app
//= require dispatcher
//= require_tree ./controllers
//= require_tree ./mobile
//= require_tree ./features
//= require_self

$(APP.init);
