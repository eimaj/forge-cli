resources :video_feeds, :except => [:show, :destroy] do
  get 'publish', :on => :member
end
